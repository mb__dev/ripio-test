import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import {Provider} from 'react-redux';
import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'

import AppNavigator from './src/navigation/AppNavigator';
import RootReducer from './src/reducers/index';

const store = createStore(RootReducer, applyMiddleware(thunk))


export default function App() {
  return (
    <Provider store={store}>
      <AppNavigator />
    </Provider>
  );
}
