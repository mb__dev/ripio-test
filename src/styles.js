import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({

    container: {
      flex: 1,
      justifyContent: 'flex-start',
      color: '#fff',
      backgroundColor: '#2b2b2b',
      alignItems:'center',
      paddingTop:20
    },

    hContainer: {
      flex: 1,
      padding:20
    },

    mainStatusContainer: {
      padding: 20,
      backgroundColor:'#1c1c1c',
      alignContent: 'center',
      width: '80%',
    },

    mainStatusLabel: {
      alignContent:'center',
      justifyContent:'center',
      textAlign: 'center',
      color:'#d4d4d4',
      fontSize: 20
    },
    
    mainStatusBalance: {
      textAlign: 'center',
      color:'#f2f2f2',
      padding:10,
      fontSize:30
    },

    sendBlockContainer: {
      marginTop: 20,
      padding: 20,
      backgroundColor:'#1c1c1c',
      alignContent: 'center',
      width: '80%',
    },
  
    sendBlockLabel: {
      color:'#d4d4d4',
      fontSize: 20
    },

    input: {
      height: 40,
      margin: 12,
      borderWidth: 1,
      padding: 10,
      color:'white',
      borderColor:'white'
    },

    card: {
      marginBottom:20
    },
    
    cardTitle: {
      fontSize:20,
    }
    
});

export default styles;