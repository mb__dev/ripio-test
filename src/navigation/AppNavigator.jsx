import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Home from '../screens/Home';
import History from '../screens/History';

const Tab = createBottomTabNavigator();

function AppNavigator() {
  return (
    <NavigationContainer>
        <Tab.Navigator>
        <Tab.Screen name="Home" component={Home} />
        <Tab.Screen name="Historial" component={History} />
        </Tab.Navigator>
    </NavigationContainer>
  );
}

export default AppNavigator;