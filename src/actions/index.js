export const SET_BTC_RATE = 'SET_BTC_RATE';
export const UPDATE_BALANCE = 'UPDATE_BALANCE';
export const ADD_TRANSACTION = 'ADD_TRANSACTION';

export const fetchCoinData = () => {
    return async (dispatch, getState) => {
        try {
            const cryptoResponse = await fetch(
                `https://ripio.com/api/v1/rates/`
              );
            const cryptoResponseData = await cryptoResponse.json();
            dispatch({
                type: SET_BTC_RATE,
                btcRate: cryptoResponseData.rates.ARS_SELL
            });
        } catch (error) {
            console.log(error);
        }
    }
}

export const sendBtc = (amount, address) => {
    return async (dispatch, getState) => {
        const transaction = {
            date: new Date().toISOString().slice(0,10),
            amount,
            address,
            status: 1, // For practical purposes
            id: Math.random().toFixed(3) // For practical purposes
        }

        dispatch({
            type: ADD_TRANSACTION,
            transaction
        });
        dispatch({
            type: UPDATE_BALANCE,
            amount
        });
        dispatch(fetchCoinData());
    }
}