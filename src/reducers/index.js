
import { SET_BTC_RATE, UPDATE_BALANCE, ADD_TRANSACTION } from '../actions/index';

const initialState = {
    balance: 0.980000,
    balance_ars: 0,
    history: []
}


export default (state = initialState, action) => {
    switch (action.type) {
        case SET_BTC_RATE:
            return {
               ...state, balance_ars: action.btcRate * state.balance
            }
        case UPDATE_BALANCE: // Supongo que siempre es una resta para simplificar
            return {
                ...state,
                balance: state.balance - action.amount
            }
        case ADD_TRANSACTION: {
            return {
                ...state,
                history: [...state.history, action.transaction]
            }

        }
        default:
            return state;
    }
}