import React, { useEffect } from 'react';
import { useIsFocused } from '@react-navigation/native';
import { Text, KeyboardAvoidingView, View, TextInput, Button, Alert } from 'react-native';
import { useSelector, useDispatch } from 'react-redux'
import * as coinActions from '../actions/index';
import styles from '../styles';

export default function Home() {

 const state = useSelector(state => state);
 const dispatch = useDispatch();
 const isFocused = useIsFocused();

 useEffect(() => {
    if(!isFocused) return;
    dispatch(coinActions.fetchCoinData());
  },[isFocused]);

  const [amountToSend, onChangeAmountToSend] = React.useState(0);
  const [addressToSend, onChangeAddressToSend] = React.useState(0);

  const sendBtcToAddress = () => {
    const randomFee = randomValueFromInterval(0.0001, 0.0002);
    const finalAmountToSend = parseFloat(amountToSend) + randomFee;

    if(!amountToSend || !addressToSend) {
      Alert.alert('Complete importe y dirección de envío');
      return;
    }

    if (finalAmountToSend > state.balance) {
      Alert.alert('Saldo insuficiente');
      return;
    }
    dispatch(coinActions.sendBtc(finalAmountToSend, addressToSend));
    Alert.alert('Transacción finalizada con éxito');
  }

  const randomValueFromInterval = (min, max) => {
    return Math.random() * (max - min) + min;
  }

  return (
    <KeyboardAvoidingView style={styles.container}>
        <View style={styles.mainStatusContainer}>
            <Text style={styles.mainStatusLabel}>Balance</Text>
            <Text style={styles.mainStatusBalance}>{state.balance.toFixed(4)} BTC</Text>
            <Text style={styles.mainStatusLabel}>{`$ ${state.balance_ars.toFixed(2)} (ARS)`}</Text>
        </View>

        <View  behavior={Platform.OS === "ios" ? "padding" : "height"} style={styles.sendBlockContainer}>
          <Text style={styles.sendBlockLabel}>Enviar:</Text>
          <TextInput 
            style={styles.input} 
            onChangeText={onChangeAmountToSend} 
            value={amountToSend} 
            keyboardType="numeric"
          />
          <Text style={styles.sendBlockLabel}>a:</Text>
          <TextInput 
            style={styles.input} 
            onChangeText={onChangeAddressToSend} 
            value={addressToSend} 
            keyboardType="numeric"
          />
          <Button
            title="Enviar"
            onPress={sendBtcToAddress} 
          />
        </View>
    </KeyboardAvoidingView>
  )
}