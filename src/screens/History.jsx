import React from 'react'
import { Text, SafeAreaView, ScrollView, View } from 'react-native';
import { useSelector } from 'react-redux'
import styles from '../styles';

export default function History() {

  const state = useSelector(state => state);

  function RenderTransactions() 
  {
    if(!state.history || !state.history.length)
    { 
      return <Text>No hay transacciones.</Text>;
    }

    const history = state.history.map((t) => 
      <View key={t.id} style={styles.card}>
        <Text style={styles.cardTitle}>Transacción: {t.id}</Text>
        <Text >Monto: {t.amount}</Text>
        <Text >Fecha: {t.date}</Text>
        <Text >Dirección: {t.address}</Text>
        <Text >Estado: {t.status}</Text>
      </View>
    ); 
    return history;
  }

  return (
    <SafeAreaView style={styles.hContainer}>
      <ScrollView style={styles.scrollView}>
        <Text style={{borderBottomWidth:1, marginBottom:20}}>Historial</Text>
        <RenderTransactions />
      </ScrollView>
    </SafeAreaView>
  )
}
